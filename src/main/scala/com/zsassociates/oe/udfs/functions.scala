package com.zsassociates.oe.udfs

import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.Calendar
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema
import scala.collection.JavaConversions._
import scala.collection.{JavaConversions, mutable}
import scala.collection.mutable.{ArrayBuffer}
import scala.util.control.Breaks
import scala.reflect.ClassTag
import RowExtensions._

object RowExtensions {
  implicit def extensions(row: GenericRowWithSchema) = new {
    def getField[A: ClassTag](fieldName: String) = {
      Option(row(row.fieldIndex(fieldName))) match {
        case Some(s: A) => s.asInstanceOf[A]
        case Some(x) =>
          throw new IllegalArgumentException(
            s"type mismatch. found ${x.getClass}")
        case None =>
          throw new IllegalArgumentException(s"non exisitng field, $fieldName")
      }
    }
    def getFieldOrElse[A: ClassTag](fieldName: String, defaultValue: A) = {
      Option(row(row.fieldIndex(fieldName))) match {
        case Some(s: A) => s.asInstanceOf[A]
        case Some(x) =>
          throw new IllegalArgumentException(
            s"type mismatch. found ${x.getClass}")
        case None =>
          defaultValue
      }
    }
  }
}


/**
  * Created by kiran on 8/14/16.
  */
object SelfTestType extends Enumeration {
  type SelfTestType = Value
  val SingleFactorAnova = Value("SingleFactorAnova")
  val CriticalPercentageFactor = Value("CriticalPercentageFactor")
  val HeteroscedasticTTest = Value("HeteroscedasticTTest")
  val HomoscedasticTTest = Value("HomoscedasticTTest")
  val None = Value("None")
}

object functions {

  var contentRecommendationRawToObject = (content_recommendation_raw: GenericRowWithSchema) =>{
    Reccomendation(
      content_recommendation_raw.getField[String]("channelId"),
      content_recommendation_raw.getField[Timestamp]("date"),
      content_recommendation_raw.getField[String]("contentId"),
      null,
      content_recommendation_raw.getField[String]("rationale")
    )
  }

  var fixedEventsRawToObject = (fixed_events : mutable.WrappedArray[AnyRef]) => {
    val mlDateFormat = new SimpleDateFormat("MM/dd/yyyy")
    fixed_events
      .asInstanceOf[Seq[GenericRowWithSchema]]
      .map(c => {
        FixedEvent(
          c.getField[String]("CONTENT"),
          c.getField[String]("CONTENT_TYPE"),
          new Timestamp(
            mlDateFormat.parse(c.getField[String]("DATE")).getTime),
          Some(c.getField[Int]("RANGE")),
          c.getField[String]("CHANNEL"),
          c.getField[String]("FIXED_EVENT_NAME"),
          c.getField[Boolean]("IS_REQUIRED_SEQ"),
          c.getField[String]("ACTION"),
          c.getField[String]("REQ_SEQ_CHANNEL")
        )
      })
  }

  var multiDimensionalFixedEventsRawToObject = (fixed_events:  mutable.WrappedArray[AnyRef]) => {
    val mlDateFormat = new SimpleDateFormat("MM/dd/yyyy")
    fixed_events
      .asInstanceOf[Seq[GenericRowWithSchema]]
      .filter(f => f != null)
      .map(c => {
        MultiDimensionFixedEvent(
          c.getAs[String]("CONTENT"),
          c.getAs[String]("DIMENSION1"),
          new Timestamp(
            mlDateFormat.parse(c.getAs[String]("DATE")).getTime),
          Some(c.getAs[Int]("RANGE")),
          c.getFieldOrElse[String]("DIMENSION2", null),
          c.getAs[String]("FIXED_EVENT_NAME"),
          c.getField[Boolean]("IS_REQUIRED_SEQ"),
          c.getField[String]("ACTION"),
          c.getField[String]("REQ_SEQ_CHANNEL")
        )


      })
  }


  var fixedEventsRawToNamedStructObj = (fixed_events : mutable.WrappedArray[AnyRef]) => {
    fixed_events
      .asInstanceOf[Seq[GenericRowWithSchema]]
      .map(c => {
        FixedEventNamedStruct(
          c.getField[String]("CHANNEL"),
          c.getField[String]("CONTENT"),
          c.getField[String]("CONTENT_TYPE"),
          c.getField[String]("DATE"),
          Some(c.getField[Int]("RANGE")),
          c.getField[String]("FIXED_EVENT_NAME"),
          c.getField[Boolean]("IS_REQUIRED_SEQ"),
          c.getField[String]("ACTION"),
          c.getField[String]("REQ_SEQ_CHANNEL")
        )
      })
  }

  var fourCsRawToObject = (fourCList : mutable.WrappedArray[AnyRef]) =>
  {
    val mlDateFormat = new SimpleDateFormat("MM/dd/yyyy")
    fourCList.asInstanceOf[Seq[GenericRowWithSchema]].map(c => {
      External4c(
        c.getField[String]("content_id"),
        c.getField[String]("content_type_id"),
        new Timestamp(
          mlDateFormat.parse(c.getField[String]("cadence")).getTime),
        c.getField[String]("channel_id"),
        c.getField[String]("trigger_name")
      )
    })
  }

  def isDateInBetween (date : Timestamp,
                       startDate : Timestamp,
                       range : Option[Int]) : Boolean = {
    val cal = Calendar.getInstance
    val daysCount = range.getOrElse(0)
    cal.setTimeInMillis(startDate.getTime)
    cal.add(Calendar.DAY_OF_MONTH, daysCount - 1)
    val endDate = new Timestamp(cal.getTime.getTime)
    isDateInBetween(date,startDate,endDate)
  }

  def isDateInBetween (date : Timestamp,
                       startDate : Timestamp,
                       endDate : Timestamp) : Boolean = {
    ((date.equals(startDate) || date.after(startDate))
      && (date.equals(endDate) || date.before(endDate)))
  }

  var getMLRationaleInfo = (
                             content_recommendation_raw: GenericRowWithSchema,
                             external4cs: mutable.WrappedArray[AnyRef],
                             input_fixed_events: mutable.WrappedArray[AnyRef]) => {
    var is4cFixedEvent = false
    var isInputFixedEvent = false
    var rationaleKey: String = null
    var rationaleText: String = null
    var isRequiredSeq: Boolean = false

    val content_recommendation = contentRecommendationRawToObject(content_recommendation_raw)

    /**
      * Check if the recommendation is a flexi-event, set the rationale text same as fixed event name
      */
    if (input_fixed_events != null) {
      val flexiEventsList = fixedEventsRawToObject(input_fixed_events)
      flexiEventsList.find(
        fe =>
          fe.channelId == content_recommendation.channelId
            && fe.contentId == content_recommendation.contentId
            && fe.range.getOrElse(0) > 0
            && isDateInBetween(content_recommendation.date,fe.date,fe.range)) match {
        //SOME = Is an input flexi-event
        case Some(c) =>
          isInputFixedEvent = true
          is4cFixedEvent = false
          rationaleKey = content_recommendation.rationale
          rationaleText = c.fixedEventName
        case None =>
          isInputFixedEvent = false
          is4cFixedEvent = false
      }
    }

    if (input_fixed_events != null) {
      val fixedEventsList = fixedEventsRawToObject(input_fixed_events)
      fixedEventsList.find(
        fe =>
          fe.channelId == content_recommendation.channelId
            && fe.contentId == content_recommendation.contentId
            && fe.date == content_recommendation.date) match {
        //SOME = Is an input fixed event
        case Some(c) =>
          isRequiredSeq = c.isRequiredSeq
          isInputFixedEvent = true
          is4cFixedEvent = false
          if (c.isRequiredSeq) {
            rationaleKey = "EXPERT_GUIDANCE"
            rationaleText =
              if (c.action == "Engaged")
                "HCP recently engaged with " + c.reqSeqChannel
              else "HCP recently exposed to " + c.reqSeqChannel
          } else {
            rationaleKey = content_recommendation.rationale
            rationaleText = c.fixedEventName
          }
        case None =>
          if(!isInputFixedEvent)
            rationaleText="Based on HCP's recent exposure/engagement"; // setting rationaleText as hard coded string to support rationale over-laying in case of required sequences
      }
    }
    if (external4cs != null) {
      val external4csList = fourCsRawToObject(external4cs)
      external4csList.find(
        e4c =>
          e4c.channelId == content_recommendation.channelId
            && e4c.contentId == content_recommendation.contentId
            && e4c.date == content_recommendation.date) match {
        //SOME = Is a 4c
        case Some(c) =>
          is4cFixedEvent = true
          isInputFixedEvent = false
          rationaleKey = "EXPERT_GUIDANCE"
          rationaleText = c.triggerName
        case None =>
          is4cFixedEvent = false
      }
    }

    if (is4cFixedEvent) {
      MLRationaleInfo(rationaleKey, rationaleText, null)
    } else if (isInputFixedEvent) {
      MLRationaleInfo(rationaleKey, null, rationaleText)
    } else {
      MLRationaleInfo(content_recommendation.rationale, null, rationaleText)
    }
  }


  var getMultiDimensionalMLRationaleInfo = (content_recommendation_raw: GenericRowWithSchema,
                             external4cs: mutable.WrappedArray[AnyRef],
                             input_fixed_events: mutable.WrappedArray[AnyRef],
                             input_flexi_events: mutable.WrappedArray[AnyRef]) => {
    var is4cFixedEvent = false
    var isInputFixedEvent = false
    var rationaleKey: String = null
    var rationaleText: String = null
    var isRequiredSeq: Boolean = false
    val mlDateFormat = new SimpleDateFormat("MM/dd/yyyy")

    val content_type_recommended = content_recommendation_raw.getField[GenericRowWithSchema]("content_type_recommended")

    val content_recommendation = DimensionBasedRecommendation(
      content_recommendation_raw.getField[Timestamp]("date"),
      content_recommendation_raw.getField[String]("rationale"),
      content_recommendation_raw.getField[String]("dimension1"),
      content_recommendation_raw.getFieldOrElse[String]("dimension2", null),
      content_recommendation_raw.getFieldOrElse[String]("channelId", null),
      content_recommendation_raw.getFieldOrElse[String]("contentType", null),
      content_recommendation_raw.getFieldOrElse[String]("product", null),
      content_recommendation_raw.getFieldOrElse[String]("indication", null),
      content_recommendation_raw.getFieldOrElse[String]("contentId", null),
      ContentType(content_type_recommended.getField[String]("id"), content_type_recommended.getField[String]("name"))
    )

    /**
      * Check if the recommendation is a flexi-event, set the rationale text same as fixed event name
      */
    if (input_flexi_events != null) {
      val flexiEventsList = multiDimensionalFixedEventsRawToObject(input_flexi_events)

      flexiEventsList.find(
        fe =>
          fe.dimension1 == content_recommendation.dimension1
            && fe.dimension2 == content_recommendation.dimension2
            && fe.contentId == content_recommendation.contentId
            && isDateInBetween(content_recommendation.date,fe.date,fe.range)) match {
        //SOME = Is an input flexi-event
        case Some(c) =>
          isInputFixedEvent = true
          is4cFixedEvent = false
          rationaleKey = content_recommendation.rationale
          rationaleText = c.fixedEventName
        case None =>
          isInputFixedEvent = false
          is4cFixedEvent = false
      }
    }

    if (input_fixed_events != null) {
      val fixedEventsList = multiDimensionalFixedEventsRawToObject(input_fixed_events)

      fixedEventsList.find(
        fe =>
          fe.dimension1 == content_recommendation.dimension1
            && fe.dimension2 == content_recommendation.dimension2
            && fe.contentId == content_recommendation.contentId
            && fe.date == content_recommendation.date) match {
        //SOME = Is an input fixed event
        case Some(c) =>
          isRequiredSeq = c.isRequiredSeq
          isInputFixedEvent = true
          is4cFixedEvent = false
          if (c.isRequiredSeq) {
            rationaleKey = "EXPERT_GUIDANCE"
            rationaleText =
              if (c.action == "Engaged")
                "HCP recently engaged with " + c.reqSeqChannel
              else "HCP recently exposed to " + c.reqSeqChannel
          } else {
            rationaleKey = content_recommendation.rationale
            rationaleText = c.fixedEventName
          }
        case None =>
          if(!isInputFixedEvent)
            rationaleText="Based on HCP's recent exposure/engagement"; // setting rationaleText as hard coded string to support rationale over-laying in case of required sequences
      }
    }
    if (external4cs != null) {
      val external4csList = fourCsRawToObject(external4cs)
      external4csList.find(
        e4c =>
          e4c.channelId == content_recommendation.channelId
            && e4c.contentTypeId == content_recommendation.content_type_recommended.id
            && e4c.contentId == content_recommendation.contentId
            && e4c.date == content_recommendation.date) match {
        //SOME = Is a 4c
        case Some(c) =>
          is4cFixedEvent = true
          isInputFixedEvent = false
          rationaleKey = "EXPERT_GUIDANCE"
          rationaleText = c.triggerName
        case None =>
          is4cFixedEvent = false
      }
    }

    if (is4cFixedEvent) {
      MLRationaleInfo(rationaleKey, rationaleText, null)
    } else if (isInputFixedEvent) {
      MLRationaleInfo(rationaleKey, null, rationaleText)
    } else {
      MLRationaleInfo(content_recommendation.rationale, null, rationaleText)
    }
  }


  case class MinMaxGAllowedTP(DIMENSION_VALUE: String,
                              MIN_ALLOWED: Integer,
                              MAX_ALLOWED: Integer,
                              GRANULARITY: String,
                              DIMENSION_TYPE: String)

  def minMaxAllowedTPRawToNamedStructObj(minMaxAllowedTP : mutable.WrappedArray[AnyRef]): List[MinMaxGAllowedTP] = {
    if(minMaxAllowedTP == null)
      List[MinMaxGAllowedTP]()
    else
      minMaxAllowedTP
        .asInstanceOf[Seq[GenericRowWithSchema]]
        .map(c => {
          MinMaxGAllowedTP(
            c.getField[String]("DIMENSION_VALUE"),
            c.getField[Int]("MIN_ALLOWED"),
            c.getField[Int]("MAX_ALLOWED"),
            c.getField[String]("GRANULARITY"),
            c.getField[String]("DIMENSION_TYPE"))
        }).toList
  }

  /**
    * completeMinMaxAllowedTouchpoints returns the array of all MinMaxAllowedTouchpoints combinations
    * complete combinations includes all combinations in input and missing combinations which are required
    */

  var completeMinMaxAllowedTouchpoints = (minMaxAllowedTP : mutable.WrappedArray[AnyRef],
                                          dimension1LOVs : mutable.WrappedArray[String],
                                          dimension2LOVs : mutable.WrappedArray[String],
                                          defaultMinAllowed : Int,
                                          defaultMaxAllowed : Int) => {

      val actualMinMaxAllowedTP = minMaxAllowedTPRawToNamedStructObj(minMaxAllowedTP)
      var dimension1LOVMap = dimension1LOVs.map(x => x -> "DIMENSION1").toMap
      var dimension2LOVMap = dimension2LOVs.map(x => x -> "DIMENSION2").toMap

      actualMinMaxAllowedTP.map(item => {
        if (item.DIMENSION_TYPE.equalsIgnoreCase("DIMENSION1"))
          dimension1LOVMap -= item.DIMENSION_VALUE
        if (item.DIMENSION_TYPE.equalsIgnoreCase("DIMENSION2"))
          dimension2LOVMap -= item.DIMENSION_VALUE
      })

      val defaultGranularity =
        if (actualMinMaxAllowedTP.length>0)
          actualMinMaxAllowedTP.get(0).GRANULARITY
        else
          s"Monthly"

      dimension2LOVMap -= "GENERAL" //If this is 2d mode of 3d remove GENERAL, it will be calculated later
      val missingMinMaxAllowedTP =  (dimension1LOVMap ++ dimension2LOVMap).map(item => {
        MinMaxGAllowedTP(item._1,defaultMinAllowed,defaultMaxAllowed,defaultGranularity,item._2)
      })

      //return complete minMaxAllowedTP which is combined list of actual items & missing items
      actualMinMaxAllowedTP ++ missingMinMaxAllowedTP
  }

  /**
    * Filters the fixed event array with cadence greater than or equal to the input date
    * Returns the filtered array
    */
  var filterFixedEventsByDate = (fixedEventsList : mutable.WrappedArray[AnyRef],
                                 date : String) => {
    val mlDateFormat = new SimpleDateFormat("MM/dd/yyyy")
    if (fixedEventsList == null)
      null
    else {
      val fixedEvents = fixedEventsRawToNamedStructObj(fixedEventsList)
      val filterDate = new Timestamp(mlDateFormat.parse(date).getTime)
      fixedEvents.filter(fe => {
        val dt = new Timestamp(mlDateFormat.parse(fe.DATE).getTime)
        (dt.equals(filterDate) || dt.after(filterDate))
      })
    }
  }



  val getContentType =
    (contentId: String, contentList: mutable.WrappedArray[AnyRef]) => {

      val contents = contentList.map(content => {
        val c = content.asInstanceOf[GenericRowWithSchema]

        Content(
          c.getField[String]("contentId"),
          c.getField[String]("contentTypeId"),
          c.getField[Int]("seq"),
          c.getAs[Int]("maxRetries"),
          c.getField[Timestamp]("startDate"),
          c.getField[Timestamp]("endDate"),
          c.getField[String]("channelId")
        )
      })

      contents.find(x => x.id.equals(contentId)) match {
        case Some(c) => c.contentTypeId
        case None    => null
      }

    }

  case class MLRationaleInfo(rationaleKey: String,
                             triggerName: String,
                             fixedEventName: String)

  case class Content(id: String,
                     contentTypeId: String,
                     seq: Int,
                     maxRetries: Int,
                     startDate: Timestamp,
                     endDate: Timestamp,
                     channelId: String
                    )


  case class FixedEvent(contentId: String,
                        contentTypeId: String,
                        date: Timestamp,
                        range: Option[Int] = None,
                        channelId: String,
                        fixedEventName: String,
                        isRequiredSeq: Boolean,
                        action: String,
                        reqSeqChannel: String)

  case class FixedEventNamedStruct(CHANNEL: String,
                                   CONTENT: String,
                                   CONTENT_TYPE: String,
                                   DATE: String,
                                   RANGE: Option[Int] = None,
                                   FIXED_EVENT_NAME: String,
                                   IS_REQUIRED_SEQ: Boolean,
                                   ACTION: String,
                                   REQ_SEQ_CHANNEL: String)

  case class ContentType(id: String, name: String)

  case class Reccomendation(channelId: String,
                            date: Timestamp,
                            contentId: String,
                            content_type_recommended: ContentType,
                            rationale: String) {
    def withContent(_contentId: String, _contentType: ContentType) =
      Reccomendation(channelId, date, _contentId, _contentType, rationale)
  }

  case class DimensionEntity(channel: String,
                             contentType: String,
                             product: String,
                             indication: String)

  case class ConcreteRecommendation(date: Timestamp, rationale: String, channel: String, content: String, product: String, indication: String)

  case class DimensionBasedRecommendation(date : Timestamp,
                                          rationale: String,
                                          dimension1: String,
                                          dimension2: String,
                                          channelId: String,
                                          contentType: String,
                                          product: String,
                                          indication: String,
                                          contentId: String,
                                          content_type_recommended: ContentType){
    def withEntities(_channelId: String, _contentTypeId: String, _productId: String, _indication: String) =
      DimensionBasedRecommendation(date, rationale, dimension1, dimension2, _channelId, _contentTypeId, _productId, _indication, contentId, content_type_recommended)

    def withContent(_contentId: String, _contentType: ContentType) =
      DimensionBasedRecommendation(date, rationale, dimension1, dimension2, channelId, contentType, product, indication, _contentId, _contentType)
  }

  case class MultiDimensionFixedEvent(contentId: String,
                                      dimension1: String,
                                      date: Timestamp,
                                      range: Option[Int] = None,
                                      dimension2: String,
                                      fixedEventName: String,
                                      isRequiredSeq: Boolean,
                                      action: String,
                                      reqSeqChannel: String)

  case class DimensionElement(element : List[String])

  case class ModelDimension(list: List[DimensionElement])

  case class LastContentCount(content_type: String,
                              var contentId: String,
                              var count: Int)

  case class PastEvent(ContentType: String, contentId: String, date: Timestamp)

  case class External4c(contentId: String,
                        contentTypeId: String,
                        date: Timestamp,
                        channelId: String,
                        triggerName: String)



  val getLastDateOfRange=(startDate:Timestamp,range:Int)=>{
    val cal = Calendar.getInstance
    cal.setTimeInMillis(startDate.getTime)
    cal.add(Calendar.DAY_OF_MONTH, range-1)
    val marketingMappingInputEndDate = new Timestamp(cal.getTime().getTime())
    marketingMappingInputEndDate
  }

  val getContent = (next_content_types: mutable.WrappedArray[String],
                    ml_recommended_channels: mutable.WrappedArray[AnyRef],
                    contentList: mutable.WrappedArray[AnyRef],
                    fixedEventsList: mutable.WrappedArray[AnyRef],
                    contentTypeList: mutable.WrappedArray[AnyRef],
                    pastContentInteraction:mutable.WrappedArray[AnyRef],
                    marketingMappingInputsList:mutable.WrappedArray[AnyRef]
                   ) => {


    val mlDateFormat = new SimpleDateFormat("MM/dd/yyyy")
    val loop = new Breaks;

    val marketingMappingInputs=if(marketingMappingInputsList!=null && marketingMappingInputsList.length>0)
      marketingMappingInputsList.map(p=>{
        val m=p.asInstanceOf[GenericRowWithSchema]
        FixedEvent(
          m.getAs[String]("CONTENT"),
          m.getAs[String]("CONTENT_TYPE"),
          new Timestamp(
            mlDateFormat.parse(m.getAs[String]("DATE")).getTime),
          Some(m.getAs[Int]("RANGE")),
          m.getAs[String]("CHANNEL"),
          m.getAs[String]("FIXED_EVENT_NAME"),
          m.getField[Boolean]("IS_REQUIRED_SEQ"),
          m.getField[String]("ACTION"),
          m.getField[String]("REQ_SEQ_CHANNEL")
        )
      })
    else
      Seq.empty[FixedEvent]

    val pastContentInteractionList=
      pastContentInteraction.map(p=>{
        val pastEvent=p.asInstanceOf[GenericRowWithSchema]
        PastEvent(
          pastEvent.getAs[String]("CONTENT_TYPE"),
          pastEvent.getAs[String]("CONTENT"),
          new Timestamp(
            mlDateFormat.parse(pastEvent.getAs[String]("DATE")).getTime)
        )
      })
    val unsortedfixedEvents =
      if (fixedEventsList != null)
        fixedEventsList
          .asInstanceOf[Seq[GenericRowWithSchema]]
          .filter(f => f != null)
          .map(c => {
            FixedEvent(
              c.getAs[String]("CONTENT"),
              c.getAs[String]("CONTENT_TYPE"),
              new Timestamp(
                mlDateFormat.parse(c.getAs[String]("DATE")).getTime),
              Some(c.getAs[Int]("RANGE")),
              c.getAs[String]("CHANNEL"),
              c.getAs[String]("FIXED_EVENT_NAME"),
              c.getField[Boolean]("IS_REQUIRED_SEQ"),
              c.getField[String]("ACTION"),
              c.getField[String]("REQ_SEQ_CHANNEL")
            )
          })
      else Seq.empty[FixedEvent]
    var contents = contentList.map(content => {
      val c = content.asInstanceOf[GenericRowWithSchema]

      Content(
        c.getAs[String]("contentId"),
        c.getAs[String]("contentTypeId"),
        c.getAs[Int]("seq"),
        c.getAs[Int]("maxRetries"),
        c.getAs[Timestamp]("startDate"),
        c.getAs[Timestamp]("endDate"),
        c.getAs[String]("channelId")
      )
    })
    contents=contents.filter(x=>x.maxRetries>0)
    val contentTypes = contentTypeList.map(contentType => {
      val k = contentType.asInstanceOf[GenericRowWithSchema]
      ContentType(k.getAs[String]("id"), k.getAs[String]("name"))
    })
    val recommendations = ml_recommended_channels.map(c => {
      val channel = c.asInstanceOf[GenericRowWithSchema]
      Reccomendation(
        channel.getAs[String]("CHANNEL"),
        new Timestamp(
          mlDateFormat.parse(channel.getAs[String]("DATE")).getTime),
        null,
        null,
        channel.getAs[String]("RATIONALE"))
    })
    val lastContentsCountList : List[LastContentCount]= pastContentInteractionList.groupBy(c=>c.ContentType).map(x=>{
      val contentType=x._1
      val contents=x._2.sortWith((s1, s2) => {
        s1.date.after(s2.date)
      })
      val lastContent=contents(0).contentId
      val count=contents.count(c=>c.contentId.equals(lastContent))
      LastContentCount(contentType,lastContent,count)
    }).toList

    if (next_content_types == null) {
      mutable.WrappedArray.empty[Reccomendation]
    } else {
      val next_content_type_reversemap = next_content_types.zipWithIndex.toMap
      val contentMapByChannels = contents
        .filter(c => next_content_types.contains(c.contentTypeId))
        .groupBy(c => c.channelId)
        .map(x => {
          val channelId = x._1
          val contents = x._2.sortWith((s1, s2) => {
            next_content_type_reversemap(s1.contentTypeId) < next_content_type_reversemap(
              s2.contentTypeId)

          })
          (channelId,contents)
        })
      val contentMapByChannelContentType =contentMapByChannels.map(x => {
        val channelId = x._1
        var contentsByChannel = x._2
        val contentsMapByContentCodes=
          contentsByChannel
            .groupBy(c => c.contentTypeId).toList.sortWith((s1,s2)=>{next_content_type_reversemap(s1._1) < next_content_type_reversemap(
            s2._1)})
            .map(y => {
              val contentTypeId = y._1
              val contents = y._2.sortWith((s1, s2) => {
                s1.seq < s2.seq
              })
              (contentTypeId,contents)
            })
        (channelId,contentsMapByContentCodes)
      }
      )
      var consumedContentTypes: List[String] = Nil
      var consumedFixedEvents: List[FixedEvent] = Nil;
      //sorting fixedEvents list to evaluate Fixed Events first.
      var fixedEvents=unsortedfixedEvents.sortBy(x=>x.range)

      recommendations
        .map(r => {
          var recommendationWithContent: Reccomendation = null
          if (r.rationale.equals("FIXED_EVENT")) {
            loop.breakable {
              for (fixedevent <- fixedEvents) {
                if (fixedevent.channelId == r.channelId &&
                  fixedevent.date == r.date &&
                  fixedevent.contentId != "" &&
                  fixedevent.range == Some(0) &&
                  !consumedFixedEvents.contains(fixedevent)) {
                  //Nba native and 4c fixed events
                  consumedFixedEvents = consumedFixedEvents :+ fixedevent
                  contentTypes
                    .find(k => k.id.equalsIgnoreCase(fixedevent.contentTypeId)) match {
                    case Some(cc) => {
                      recommendationWithContent =
                        r.withContent(fixedevent.contentId, cc)
                      loop.break()
                    }
                    case None => null
                  }
                } else if (fixedevent.channelId == r.channelId
                  && fixedevent.contentId == ""
                  && fixedevent.range == Some(0)
                  && fixedevent.date == r.date
                  && fixedevent.isRequiredSeq == true
                  && !consumedFixedEvents.contains(fixedevent)) {
                  //Required Sequences
                  contentMapByChannelContentType.get(r.channelId) match {
                    case Some(c) =>
                      c.filter(content =>
                        !consumedContentTypes
                          .contains(content._1))
                      match {
                        case first :: _ =>
                          consumedContentTypes = consumedContentTypes :+ first._1
                          val contents=first._2.filter(content =>
                            (content.startDate.equals(r.date) || content.startDate.before(r.date))
                              && (content.endDate.equals(r.date) || content.endDate.after(r.date)))
                          contentTypes.find(k =>
                            k.id.equalsIgnoreCase(first._1)) match {
                            case Some(cc) =>
                              lastContentsCountList.find(x => x.content_type.equals(cc.id))
                              match {
                                case Some(ccd) => var lastContentCode = ccd.contentId
                                  var lastCount = ccd.count
                                  var nextContentCode = ""
                                  var chooseNxt = false
                                  if (contents.length > 0) {
                                    loop.breakable {
                                      for (x <- contents) {
                                        if ((x.id.equals(lastContentCode) && (lastCount % x.maxRetries) != 0) || chooseNxt) {
                                          nextContentCode = x.id;
                                          chooseNxt = false;
                                          loop.break()
                                        }
                                        else if ((x.id.equals(lastContentCode) && (lastCount % x.maxRetries) == 0)) {
                                          chooseNxt = true;
                                        }
                                      }
                                    }
                                    if (chooseNxt == true || nextContentCode == "") {
                                      nextContentCode = contents(0).id
                                    }
                                    recommendationWithContent = r.withContent(nextContentCode, cc)
                                    loop.break()
                                  }
                                  else null
                                case None =>
                                  if (contents.length > 0) {
                                    recommendationWithContent = r.withContent(contents(0).id, cc)
                                    loop.break()
                                  }
                              }

                            case None => null
                          }
                        case Nil => null
                      }
                    case None => null
                  }
                } else if (fixedevent.channelId == r.channelId && fixedevent.range != Some(0) && fixedevent.isRequiredSeq == false && !consumedFixedEvents.contains(fixedevent)) {
                  val filteredMarketingMappingInputs = marketingMappingInputs.filter(m => m.channelId == fixedevent.channelId)
                  var overlappingHumanizationEventList = filteredMarketingMappingInputs.find(mm =>
                    ((mm.date.before(r.date) && getLastDateOfRange(mm.date, mm.range.getOrElse(0)).after(r.date)) || (mm.date == r.date) || (getLastDateOfRange(mm.date, mm.range.getOrElse(0)) == r.date))).toList;
                  if (overlappingHumanizationEventList.length > 0) {
                    loop.break()
                  }
                  else {
                    if (((fixedevent.date.before(r.date) && getLastDateOfRange(fixedevent.date, fixedevent.range.getOrElse(0)).after(r.date))
                      || (fixedevent.date == r.date) || getLastDateOfRange(fixedevent.date, fixedevent.range.getOrElse(0)).after(r.date) == fixedevent.date)&&
                      !consumedFixedEvents.contains(fixedevent)) {
                      consumedFixedEvents = consumedFixedEvents :+ fixedevent
                      contentTypes
                        .find(k => k.id.equalsIgnoreCase(fixedevent.contentTypeId)) match {
                        case Some(cc) => {
                          recommendationWithContent =
                            r.withContent(fixedevent.contentId, cc)
                          loop.break()
                        }
                        case None => null
                      }
                    }
                  }
                }
              }
            }
          } else if (r.rationale.equals("AI_DRIVEN")) {
            contentMapByChannelContentType.get(r.channelId) match {
              case Some(c) =>
                c.filter(content =>
                  !consumedContentTypes
                    .contains(content._1))
                match {
                  case first :: _ =>
                    consumedContentTypes = consumedContentTypes :+ first._1
                    val contents=first._2.filter(content =>
                      (content.startDate.equals(r.date) || content.startDate.before(r.date))
                        && (content.endDate.equals(r.date) || content.endDate.after(r.date)))
                    contentTypes.find(k =>
                      k.id.equalsIgnoreCase(first._1)) match {
                      case Some(cc) =>
                        lastContentsCountList.find(x=>x.content_type.equals(cc.id))
                        match
                        {
                          case Some(ccd)=>var lastContentCode=ccd.contentId
                            var lastCount=ccd.count
                            var nextContentCode=""
                            var chooseNxt=false
                            if(contents.length>0)
                            {
                              loop.breakable {
                                for (x <- contents) {
                                  if ((x.id.equals(lastContentCode) && (lastCount % x.maxRetries) != 0) || chooseNxt) {
                                    nextContentCode = x.id;
                                    chooseNxt = false;
                                    loop.break()

                                  }
                                  else if ((x.id.equals(lastContentCode) && (lastCount % x.maxRetries) == 0)) {
                                    chooseNxt = true;
                                  }
                                }
                              }
                              if (chooseNxt == true || nextContentCode=="") {
                                nextContentCode = contents(0).id
                              }

                              recommendationWithContent = r.withContent(nextContentCode, cc)
                            }
                            else null
                          case None =>
                            if(contents.length>0)
                            {
                              recommendationWithContent = r.withContent(contents(0).id, cc)
                            }

                        }

                      case None     => null
                    }
                  case Nil => null
                }
              case None => null
            }
          }
          recommendationWithContent
        })
        .filter(z => z != null)
    }
  }

  val getContentCode = (pastContentInteraction: mutable.WrappedArray[AnyRef],
                        ml_recommended_channels: mutable.WrappedArray[AnyRef],
                        contentList: mutable.WrappedArray[AnyRef],
                        fixedEventsList: mutable.WrappedArray[AnyRef],
                        contentTypeList: mutable.WrappedArray[AnyRef],
                        marketingMappingInputsList:mutable.WrappedArray[AnyRef]
                       ) => {

    val mlDateFormat = new SimpleDateFormat("MM/dd/yyyy")
    val loop = new Breaks;

    val marketingMappingInputs=if(marketingMappingInputsList!=null && marketingMappingInputsList.length>0)
      marketingMappingInputsList.map(p=>{
        val m=p.asInstanceOf[GenericRowWithSchema]
        FixedEvent(
          m.getAs[String]("CONTENT"),
          m.getAs[String]("CONTENT_TYPE"),
          new Timestamp(
            mlDateFormat.parse(m.getAs[String]("DATE")).getTime),
          Some(m.getAs[Int]("RANGE")),
          m.getAs[String]("CHANNEL"),
          m.getAs[String]("FIXED_EVENT_NAME"),
          m.getField[Boolean]("IS_REQUIRED_SEQ"),
          m.getField[String]("ACTION"),
          m.getField[String]("REQ_SEQ_CHANNEL")
        )
      })
    else
      Seq.empty[FixedEvent]

    val pastContentInteractionList=
      pastContentInteraction.map(p=>{
        val pastEvent=p.asInstanceOf[GenericRowWithSchema]
        PastEvent(
          pastEvent.getAs[String]("CONTENT_TYPE"),
          pastEvent.getAs[String]("CONTENT"),
          new Timestamp(
            mlDateFormat.parse(pastEvent.getAs[String]("DATE")).getTime)
        )
      })
    var lastContentsCountList : List[LastContentCount]= pastContentInteractionList.groupBy(c=>c.ContentType).map(x=>{
      val contentType=x._1
      val contents=x._2.sortWith((s1, s2) => {
        s1.date.after(s2.date)
      })
      val lastContent=contents(0).contentId
      val count=contents.count(c=>c.contentId.equals(lastContent))
      LastContentCount(contentType,lastContent,count)
    }).toList
    val unsortedFixedEvents =
      if (fixedEventsList != null)
        fixedEventsList
          .asInstanceOf[Seq[GenericRowWithSchema]]
          .filter(f => f != null)
          .map(c => {
            FixedEvent(
              c.getAs[String]("CONTENT"),
              c.getAs[String]("CONTENT_TYPE"),
              new Timestamp(
                mlDateFormat.parse(c.getAs[String]("DATE")).getTime),
              Some(c.getAs[Int]("RANGE")),
              c.getAs[String]("CHANNEL"),
              c.getAs[String]("FIXED_EVENT_NAME"),
              c.getField[Boolean]("IS_REQUIRED_SEQ"),
              c.getField[String]("ACTION"),
              c.getField[String]("REQ_SEQ_CHANNEL")
            )
          })
      else Seq.empty[FixedEvent]

    var contents = contentList.map(content => {
      val c = content.asInstanceOf[GenericRowWithSchema]
      Content(
        c.getAs[String]("contentId"),
        c.getAs[String]("contentTypeId"),
        c.getAs[Int]("seq"),
        c.getAs[Int]("maxRetries"),
        c.getAs[Timestamp]("startDate"),
        c.getAs[Timestamp]("endDate"),
        c.getAs[String]("channelId")
      )
    })
    contents=contents.filter(x=>x.maxRetries>0)
    val contentTypes = contentTypeList.map(contentType => {
      val k = contentType.asInstanceOf[GenericRowWithSchema]
      ContentType(k.getAs[String]("id"), k.getAs[String]("name"))
    })

    val recommendations = ml_recommended_channels.map(c => {
      val channel = c.asInstanceOf[GenericRowWithSchema]

      Reccomendation(
        channel.getAs[String]("CHANNEL"),
        new Timestamp(
          mlDateFormat.parse(channel.getAs[String]("DATE")).getTime),
        null,
        contentTypes.find(k =>
          k.id.equalsIgnoreCase(channel.getAs[String]("CONTENT"))) match {
          case Some(cc) => cc
          case None     => null
        },
        channel.getAs[String]("RATIONALE")
      )
    })

    val contentMapByChannels = contents
      .groupBy(c => c.channelId)
      .map(x => {
        val channelId = x._1
        val contents = x._2.sortWith((s1, s2) => {
          (s1.seq) < (s2.seq)
        })
        (channelId, contents)
      })
    val contentMapByChannelContentType =contentMapByChannels.map(x => {
      val channelId = x._1
      var contentsByChannel = x._2
      val contentsMapByContentCodes=
        contentsByChannel
          .groupBy(c => c.contentTypeId)
          .map(y => {
            val contentTypeId = y._1
            val contents = y._2.sortWith((s1, s2) => {
              s1.seq < s2.seq
            })
            (contentTypeId,contents)
          })
      (channelId,contentsMapByContentCodes)
    }
    )
    var consumedFixedEvents: List[FixedEvent] = Nil;
    //sorting fixedEvents list to evaluate Fixed Events first.
    var fixedEvents=unsortedFixedEvents.sortBy(x=>x.range)

    recommendations
      .map(r => {
        var recommendationWithContent: Reccomendation = null
        if (r.rationale.equals("FIXED_EVENT")) {
          loop.breakable {
            for (fixedevent <- fixedEvents) {
              if (fixedevent.channelId == r.channelId &&
                fixedevent.date == r.date &&
                fixedevent.contentId != "" &&
                fixedevent.range == Some(0) &&
                !consumedFixedEvents.contains(fixedevent)) {
                //Nba native and 4c fixed events
                consumedFixedEvents = consumedFixedEvents :+ fixedevent
                contentTypes
                  .find(k => k.id.equalsIgnoreCase(fixedevent.contentTypeId)) match {
                  case Some(cc) =>
                    recommendationWithContent =
                      r.withContent(fixedevent.contentId, cc)
                    loop.break()
                  case None => null
                }
              } else if (fixedevent.channelId == r.channelId &&
                fixedevent.contentId == "" &&
                fixedevent.range == Some(0) &&
                fixedevent.date == r.date &&
                fixedevent.isRequiredSeq == true &&
                !consumedFixedEvents.contains(fixedevent)) {
                //Required Sequences not allowed in Channel-Content ML model
                //null
                loop.break()
              } else if (fixedevent.channelId == r.channelId &&
                fixedevent.range != Some(0) &&
                fixedevent.isRequiredSeq == false &&
                !consumedFixedEvents.contains(fixedevent))  {
                val filteredMarketingMappingInputs = marketingMappingInputs.filter(m => (m.channelId == fixedevent.channelId))
                var overlappingHumanizationEventList = filteredMarketingMappingInputs.find(mm =>
                  (mm.date.before(r.date) && getLastDateOfRange(mm.date, mm.range.getOrElse(0)).after(r.date))
                    || (mm.date == r.date)
                    || (getLastDateOfRange(mm.date, mm.range.getOrElse(0)) == r.date)).toList;
                if (overlappingHumanizationEventList.length > 0) {
                  loop.break()
                }
                else if (((fixedevent.date.before(r.date) && getLastDateOfRange(fixedevent.date, fixedevent.range.getOrElse(0)).after(r.date))
                  || (fixedevent.date == r.date)
                  || getLastDateOfRange(fixedevent.date, fixedevent.range.getOrElse(0)) == r.date)
                  && !consumedFixedEvents.contains(fixedevent)) {
                  consumedFixedEvents = consumedFixedEvents :+ fixedevent
                  contentTypes
                    .find(k => k.id.equalsIgnoreCase(fixedevent.contentTypeId)) match {
                    case Some(cc) => {
                      recommendationWithContent =
                        r.withContent(fixedevent.contentId, cc)
                      loop.break()
                    }
                    case None => null
                  }
                }
              }
            }
          }
        } else if (r.rationale.equals("AI_DRIVEN")) {
          contentMapByChannelContentType.get(r.channelId) match {
            case Some(c) =>
              c.find(content =>
                content._1 == r.content_type_recommended.id)
              match{
                case Some(ct) =>
                  val contents=ct._2.filter(content =>
                    (content.startDate.equals(r.date) || content.startDate.before(r.date))
                      && (content.endDate.equals(r.date) || content.endDate.after(r.date)))
                  lastContentsCountList.find(x=>x.content_type.equals(r.content_type_recommended.id))
                  match
                  {
                    case Some(ccd)=>var lastContentCode=ccd.contentId
                      val lastCount=ccd.count
                      var nextContentCode=""
                      var chooseNxt=false
                      if(contents.length>0)
                      {
                        for (x <- contents)
                        {
                          if ((x.id.equals(lastContentCode) && (lastCount % x.maxRetries) != 0) || chooseNxt) {
                            nextContentCode = x.id;
                            chooseNxt = false;

                          }
                          else if ((x.id.equals(lastContentCode) && (lastCount % x.maxRetries) == 0))
                          {
                            chooseNxt = true;
                          }
                        }

                        if (chooseNxt == true || nextContentCode=="")
                        {
                          nextContentCode = contents(0).id
                        }

                        recommendationWithContent = r.withContent(nextContentCode, r.content_type_recommended)

                        val filteredLastContents=lastContentsCountList.filter(x=>x.content_type.equals(r.content_type_recommended.id))
                        if(filteredLastContents.length>0)
                        {
                          if(filteredLastContents(0).contentId.equals(nextContentCode))
                          {
                            filteredLastContents(0).count=lastCount+1
                          }
                          else
                          {

                            filteredLastContents(0).contentId=nextContentCode
                            filteredLastContents(0).count=1
                          }

                        }
                        else
                        {
                          lastContentsCountList=lastContentsCountList:+LastContentCount(r.content_type_recommended.id,nextContentCode,1)
                        }

                      }
                    case None =>
                      if(contents.length>0)
                      {
                        recommendationWithContent = r.withContent(contents(0).id, r.content_type_recommended)
                        lastContentsCountList=lastContentsCountList:+LastContentCount(r.content_type_recommended.id,contents(0).id,1)
                      }
                  }
                case None => null
              }
            case None => null
          }
        }
        recommendationWithContent
      })
      .filter(z => z != null)
  }


  val getContentMultidimensional = (dimensionsList: mutable.WrappedArray[mutable.WrappedArray[String]],
                                    dimensionSeparator: String,
                                    next_content_types: mutable.WrappedArray[String],
                                    ml_recommended_channels: mutable.WrappedArray[AnyRef],
                                    contentList: mutable.WrappedArray[AnyRef],
                                    fixedEventsList: mutable.WrappedArray[AnyRef],
                                    contentTypeList: mutable.WrappedArray[AnyRef],
                                    pastContentInteraction:mutable.WrappedArray[AnyRef],
                                    marketingMappingInputsList:mutable.WrappedArray[AnyRef]
                                   ) => {


    val mlDateFormat = new SimpleDateFormat("MM/dd/yyyy")
    val loop = new Breaks;

    val marketingMappingInputs=if(marketingMappingInputsList!=null && marketingMappingInputsList.length>0)
      multiDimensionalFixedEventsRawToObject(marketingMappingInputsList)
    else
      Seq.empty[MultiDimensionFixedEvent]

    val pastContentInteractionList=
      pastContentInteraction.map(p=>{
        val pastEvent=p.asInstanceOf[GenericRowWithSchema]
        PastEvent(
          pastEvent.getAs[String]("CONTENT_TYPE"),
          pastEvent.getAs[String]("CONTENT"),
          new Timestamp(
            mlDateFormat.parse(pastEvent.getAs[String]("DATE")).getTime)
        )
      })
    val unsortedfixedEvents =
      if (fixedEventsList != null)
        multiDimensionalFixedEventsRawToObject(fixedEventsList)
      else
        Seq.empty[MultiDimensionFixedEvent]

    var contents = contentList.map(content => {
      val c = content.asInstanceOf[GenericRowWithSchema]

      Content(
        c.getAs[String]("contentId"),
        c.getAs[String]("contentTypeId"),
        c.getAs[Int]("seq"),
        c.getAs[Int]("maxRetries"),
        c.getAs[Timestamp]("startDate"),
        c.getAs[Timestamp]("endDate"),
        c.getAs[String]("channelId")
      )
    })
    contents=contents.filter(x=>x.maxRetries>0)
    val contentTypes = contentTypeList.map(contentType => {
      val k = contentType.asInstanceOf[GenericRowWithSchema]
      ContentType(k.getAs[String]("id"), k.getAs[String]("name"))
    })

    val recommendations = ml_recommended_channels.map(p=>{
      val r = p.asInstanceOf[GenericRowWithSchema]
      val d = DimensionBasedRecommendation(new Timestamp(
        mlDateFormat.parse(r.getAs[String]("DATE")).getTime),
        r.getAs[String]("RATIONALE"),
        r.getAs[String]("DIMENSION1"),
        r.getAs[String]("DIMENSION2"),
        null,
        null,
        null,
        null,
        null,
        null)
      getEntityWithRecommendation(dimensionsList, dimensionSeparator, d)
    })

    val lastContentsCountList : List[LastContentCount]= pastContentInteractionList.groupBy(c=>c.ContentType).map(x=>{
      val contentType=x._1
      val contents=x._2.sortWith((s1, s2) => {
        s1.date.after(s2.date)
      })
      val lastContent=contents(0).contentId
      val count=contents.count(c=>c.contentId.equals(lastContent))
      LastContentCount(contentType,lastContent,count)
    }).toList

    if (next_content_types == null) {
      mutable.WrappedArray.empty[DimensionBasedRecommendation]
    } else {
      var consumedContentTypes: List[String] = Nil
      var consumedFixedEvents: List[MultiDimensionFixedEvent] = Nil;
      //sorting fixedEvents list to evaluate Fixed Events first.
      var fixedEvents=unsortedfixedEvents.sortBy(x=>x.range)

      val next_content_type_reversemap = next_content_types.zipWithIndex.toMap
      val contentMapByChannels = contents
        .filter(c => next_content_types.contains(c.contentTypeId))
        .groupBy(c => c.channelId)
        .map(x => {
          val channelId = x._1
          val contents = x._2.sortWith((s1, s2) => {
            next_content_type_reversemap(s1.contentTypeId) < next_content_type_reversemap(
              s2.contentTypeId)

          })
          (channelId,contents)
        })
      val contentMapByChannelContentType =contentMapByChannels.map(x => {
        val channelId = x._1
        var contentsByChannel = x._2
        val contentsMapByContentCodes=
          contentsByChannel
            .groupBy(c => c.contentTypeId).toList.sortWith((s1,s2)=>{next_content_type_reversemap(s1._1) < next_content_type_reversemap(
            s2._1)})
            .map(y => {
              val contentTypeId = y._1
              val contents = y._2.sortWith((s1, s2) => {
                s1.seq < s2.seq
              })
              (contentTypeId,contents)
            })
        (channelId,contentsMapByContentCodes)
      }
      )
      recommendations.map(r => {
        var recommendationWithContent: DimensionBasedRecommendation = null
        if (r.rationale.equals("AI_DRIVEN")||
          (r.rationale.equals("FIXED_EVENT") &&
            fixedEvents.exists(f => f.dimension1 == r.dimension1
              && f.dimension2 == r.dimension2
              && f.contentId == ""
              && f.range == Some(0)
              && f.date == r.date
              && f.isRequiredSeq == true
              && !consumedFixedEvents.contains(f)))) {
          contentMapByChannelContentType.get(r.channelId) match {
            case Some(c) =>
              c.filter(content =>
                !consumedContentTypes
                  .contains(content._1))
              match {
                case first :: _ =>
                  consumedContentTypes = consumedContentTypes :+ first._1
                  val contents=first._2.filter(content =>
                    (content.startDate.equals(r.date) || content.startDate.before(r.date))
                      && (content.endDate.equals(r.date) || content.endDate.after(r.date)))
                  contentTypes.find(k =>
                    k.id.equalsIgnoreCase(first._1)) match {
                    case Some(cc) =>
                      lastContentsCountList.find(x=>x.content_type.equals(cc.id))
                      match
                      {
                        case Some(ccd)=>var lastContentCode=ccd.contentId
                          var lastCount=ccd.count
                          var nextContentCode=""
                          var chooseNxt=false
                          if(contents.length>0)
                          {
                            loop.breakable {
                              for (x <- contents) {
                                if ((x.id.equals(lastContentCode) && (lastCount % x.maxRetries) != 0) || chooseNxt) {
                                  nextContentCode = x.id;
                                  chooseNxt = false;
                                  loop.break()

                                }
                                else if ((x.id.equals(lastContentCode) && (lastCount % x.maxRetries) == 0)) {
                                  chooseNxt = true;
                                }
                              }
                            }
                            if (chooseNxt == true || nextContentCode=="") {
                              nextContentCode = contents(0).id
                            }

                            recommendationWithContent = r.withContent(nextContentCode, cc)
                          }
                          else null
                        case None =>
                          if(contents.length>0)
                          {
                            recommendationWithContent = r.withContent(contents(0).id, cc)
                          }

                      }

                    case None     => null
                  }
                case Nil => null
              }
            case None => null
          }
        }
        else if (r.rationale.equals("FIXED_EVENT")) {
          loop.breakable {
            for (fixedevent <- fixedEvents) {

              var fixedEventContentType = ""
              if(fixedevent.contentId!="") {
                fixedEventContentType = contents.find(cc => cc.id.equalsIgnoreCase(fixedevent.contentId)) match {
                  case None => null
                  case Some(cc) => {
                    cc.contentTypeId
                  }
                }
              }

              if (fixedevent.dimension1 == r.dimension1 &&
                fixedevent.dimension2 == r.dimension2 &&
                fixedevent.date == r.date &&
                fixedevent.contentId != "" &&
                fixedevent.range == Some(0) &&
                !consumedFixedEvents.contains(fixedevent)) {
                //Nba native and 4c fixed events
                consumedFixedEvents = consumedFixedEvents :+ fixedevent


                contentTypes
                  .find(k => k.id.equalsIgnoreCase(fixedEventContentType)) match {
                  case Some(cc) => {
                    recommendationWithContent =
                      r.withContent(fixedevent.contentId, cc)
                    loop.break()
                  }
                  case None => null
                }
              }
              else if (fixedevent.dimension1 == r.dimension1
                && fixedevent.dimension2 == r.dimension2
                && fixedevent.range != Some(0)
                && fixedevent.isRequiredSeq == false
                && !consumedFixedEvents.contains(fixedevent)) {
                val filteredMarketingMappingInputs = marketingMappingInputs.filter(m => m.dimension1 == fixedevent.dimension1 && m.dimension2 == fixedevent.dimension2)
                var overlappingHumanizationEventList = filteredMarketingMappingInputs.find(mm =>
                  ((mm.date.before(r.date) && getLastDateOfRange(mm.date, mm.range.getOrElse(0)).after(r.date)) || (mm.date == r.date) || (getLastDateOfRange(mm.date, mm.range.getOrElse(0)) == r.date))).toList;
                if (overlappingHumanizationEventList.length > 0) {
                  loop.break()
                }
                else {
                  if (((fixedevent.date.before(r.date) && getLastDateOfRange(fixedevent.date, fixedevent.range.getOrElse(0)).after(r.date))
                    || (fixedevent.date == r.date) || getLastDateOfRange(fixedevent.date, fixedevent.range.getOrElse(0)).after(r.date) == fixedevent.date)&&
                    !consumedFixedEvents.contains(fixedevent)) {
                    consumedFixedEvents = consumedFixedEvents :+ fixedevent
                    contentTypes
                      .find(k => k.id.equalsIgnoreCase(fixedEventContentType)) match {
                      case Some(cc) => {
                        recommendationWithContent =
                          r.withContent(fixedevent.contentId, cc)
                        loop.break()
                      }
                      case None => null
                    }
                  }
                }
              }
            }
          }
        }
        recommendationWithContent
      })
        .filter(z => z != null)
    }
  }


  val getContentCodeMultiDimensional = (dimensionsList: mutable.WrappedArray[mutable.WrappedArray[String]],
                                        dimensionSeparator: String,
                                        pastContentInteraction: mutable.WrappedArray[AnyRef],
                                        ml_recommended_channels: mutable.WrappedArray[AnyRef],
                                        contentList: mutable.WrappedArray[AnyRef],
                                        fixedEventsList: mutable.WrappedArray[AnyRef],
                                        contentTypeList: mutable.WrappedArray[AnyRef],
                                        marketingMappingInputsList:mutable.WrappedArray[AnyRef]
                                       ) => {

    val mlDateFormat = new SimpleDateFormat("MM/dd/yyyy")
    val loop = new Breaks;

    val marketingMappingInputs=if(marketingMappingInputsList!=null && marketingMappingInputsList.length>0)
      multiDimensionalFixedEventsRawToObject(marketingMappingInputsList)
    else
      Seq.empty[MultiDimensionFixedEvent]

    val pastContentInteractionList=
      pastContentInteraction.map(p=>{
        val pastEvent=p.asInstanceOf[GenericRowWithSchema]
        PastEvent(
          pastEvent.getAs[String]("CONTENT_TYPE"),
          pastEvent.getAs[String]("CONTENT"),
          new Timestamp(
            mlDateFormat.parse(pastEvent.getAs[String]("DATE")).getTime)
        )
      })
    var lastContentsCountList : List[LastContentCount]= pastContentInteractionList.groupBy(c=>c.ContentType).map(x=>{
      val contentType=x._1
      val contents=x._2.sortWith((s1, s2) => {
        s1.date.after(s2.date)
      })
      val lastContent=contents(0).contentId
      val count=contents.count(c=>c.contentId.equals(lastContent))
      LastContentCount(contentType,lastContent,count)
    }).toList
    val unsortedFixedEvents =
      if (fixedEventsList != null)
        multiDimensionalFixedEventsRawToObject(fixedEventsList)
      else
        Seq.empty[MultiDimensionFixedEvent]

    var contents = contentList.map(content => {
      val c = content.asInstanceOf[GenericRowWithSchema]
      Content(
        c.getAs[String]("contentId"),
        c.getAs[String]("contentTypeId"),
        c.getAs[Int]("seq"),
        c.getAs[Int]("maxRetries"),
        c.getAs[Timestamp]("startDate"),
        c.getAs[Timestamp]("endDate"),
        c.getAs[String]("channelId")
      )
    })
    contents=contents.filter(x=>x.maxRetries>0)
    val contentTypes = contentTypeList.map(contentType => {
      val k = contentType.asInstanceOf[GenericRowWithSchema]
      ContentType(k.getAs[String]("id"), k.getAs[String]("name"))
    })

    val recommendations = ml_recommended_channels.map(p=>{
      val r = p.asInstanceOf[GenericRowWithSchema]
      val d = DimensionBasedRecommendation(new Timestamp(
        mlDateFormat.parse(r.getAs[String]("DATE")).getTime),
        r.getAs[String]("RATIONALE"),
        r.getAs[String]("DIMENSION1"),
        r.getAs[String]("DIMENSION2"),
        null,
        null,
        null,
        null,
        null,
        null)
      var dimWithEntity = getEntityWithRecommendation(dimensionsList, dimensionSeparator, d)
      dimWithEntity.withContent(null, contentTypes.find(k =>
        k.id.equalsIgnoreCase(dimWithEntity.contentType)) match {
        case Some(cc) => cc
        case None     => null
      })
    })

    var consumedFixedEvents: List[MultiDimensionFixedEvent] = Nil;
    //sorting fixedEvents list to evaluate Fixed Events first.
    var fixedEvents=unsortedFixedEvents.sortBy(x=>x.range)

    val contentMapByChannels = contents
      .groupBy(c => c.channelId)
      .map(x => {
        val channelId = x._1
        val contents = x._2.sortWith((s1, s2) => {
          (s1.seq) < (s2.seq)
        })
        (channelId, contents)
      })
    val contentMapByChannelContentType =contentMapByChannels.map(x => {
      val channelId = x._1
      var contentsByChannel = x._2
      val contentsMapByContentCodes=
        contentsByChannel
          .groupBy(c => c.contentTypeId)
          .map(y => {
            val contentTypeId = y._1
            val contents = y._2.sortWith((s1, s2) => {
              s1.seq < s2.seq
            })
            (contentTypeId,contents)
          })
      (channelId,contentsMapByContentCodes)
    }
    )

    recommendations
      .map(r => {
        var recommendationWithContent: DimensionBasedRecommendation = null
        if (r.rationale.equals("AI_DRIVEN")||
          (r.rationale.equals("FIXED_EVENT") &&
            fixedEvents.exists(f => f.dimension1 == r.dimension1
              && f.dimension2 == r.dimension2
              && f.contentId == ""
              && f.range == Some(0)
              && f.date == r.date
              && f.isRequiredSeq == true
              && !consumedFixedEvents.contains(f)))) {

          contentMapByChannelContentType.get(r.channelId) match {
            case Some(c) =>
              c.find(content =>
                content._1 == r.content_type_recommended.id)
              match{
                case Some(ct) =>
                  val contents=ct._2.filter(content =>
                    (content.startDate.equals(r.date) || content.startDate.before(r.date))
                      && (content.endDate.equals(r.date) || content.endDate.after(r.date)))
                  lastContentsCountList.find(x=>x.content_type.equals(r.content_type_recommended.id))
                  match
                  {
                    case Some(ccd)=>var lastContentCode=ccd.contentId
                      val lastCount=ccd.count
                      var nextContentCode=""
                      var chooseNxt=false
                      if(contents.length>0)
                      {
                        for (x <- contents)
                        {
                          if ((x.id.equals(lastContentCode) && (lastCount % x.maxRetries) != 0) || chooseNxt) {
                            nextContentCode = x.id;
                            chooseNxt = false;

                          }
                          else if ((x.id.equals(lastContentCode) && (lastCount % x.maxRetries) == 0))
                          {
                            chooseNxt = true;
                          }
                        }

                        if (chooseNxt == true || nextContentCode=="")
                        {
                          nextContentCode = contents(0).id
                        }

                        recommendationWithContent = r.withContent(nextContentCode, r.content_type_recommended)

                        val filteredLastContents=lastContentsCountList.filter(x=>x.content_type.equals(r.content_type_recommended.id))
                        if(filteredLastContents.length>0)
                        {
                          if(filteredLastContents(0).contentId.equals(nextContentCode))
                          {
                            filteredLastContents(0).count=lastCount+1
                          }
                          else
                          {

                            filteredLastContents(0).contentId=nextContentCode
                            filteredLastContents(0).count=1
                          }

                        }
                        else
                        {
                          lastContentsCountList=lastContentsCountList:+LastContentCount(r.content_type_recommended.id,nextContentCode,1)
                        }

                      }
                    case None =>
                      if(contents.length>0)
                      {
                        recommendationWithContent = r.withContent(contents(0).id, r.content_type_recommended)
                        lastContentsCountList=lastContentsCountList:+LastContentCount(r.content_type_recommended.id,contents(0).id,1)
                      }
                  }
                case None => null
              }
            case None => null
          }
        }
        else if (r.rationale.equals("FIXED_EVENT")) {
          loop.breakable {
            for (fixedevent <- fixedEvents) {
              var fixedEventContentType = ""
              if(fixedevent.contentId!=""){
                fixedEventContentType = contents.find(cc=> cc.id.equalsIgnoreCase(fixedevent.contentId)) match{
                  case None => null
                  case Some(cc) =>{
                    cc.contentTypeId
                  }
                }
              }

              if (fixedevent.dimension1 == r.dimension1 &&
                fixedevent.dimension2 == r.dimension2 &&
                fixedevent.date == r.date &&
                fixedevent.contentId != "" &&
                fixedevent.range == Some(0) &&
                !consumedFixedEvents.contains(fixedevent)) {
                //Nba native and 4c fixed events
                consumedFixedEvents = consumedFixedEvents :+ fixedevent
                contentTypes
                  .find(k => k.id.equalsIgnoreCase(fixedEventContentType)) match {
                  case Some(cc) =>
                    recommendationWithContent =
                      r.withContent(fixedevent.contentId, cc)
                    loop.break()
                  case None => null
                }
              }
              else if (fixedevent.dimension1 == r.dimension1 &&
                fixedevent.dimension2 == r.dimension2 &&
                fixedevent.range != Some(0) &&
                fixedevent.isRequiredSeq == false &&
                !consumedFixedEvents.contains(fixedevent))  {
                val filteredMarketingMappingInputs = marketingMappingInputs.filter(m => (fixedevent.dimension1 == m.dimension1 && fixedevent.dimension2 == m.dimension2))
                var overlappingHumanizationEventList = filteredMarketingMappingInputs.find(mm =>
                  (mm.date.before(r.date) && getLastDateOfRange(mm.date, mm.range.getOrElse(0)).after(r.date))
                    || (mm.date == r.date)
                    || (getLastDateOfRange(mm.date, mm.range.getOrElse(0)) == r.date)).toList;
                if (overlappingHumanizationEventList.length > 0) {
                  loop.break()
                }
                else if (((fixedevent.date.before(r.date) && getLastDateOfRange(fixedevent.date, fixedevent.range.getOrElse(0)).after(r.date))
                  || (fixedevent.date == r.date)
                  || getLastDateOfRange(fixedevent.date, fixedevent.range.getOrElse(0)) == r.date)
                  && !consumedFixedEvents.contains(fixedevent)) {
                  consumedFixedEvents = consumedFixedEvents :+ fixedevent
                  contentTypes
                    .find(k => k.id.equalsIgnoreCase(fixedEventContentType)) match {
                    case Some(cc) => {
                      recommendationWithContent =
                        r.withContent(fixedevent.contentId, cc)
                      loop.break()
                    }
                    case None => null
                  }
                }
              }
            }
          }
        }
        recommendationWithContent
      })
      .filter(z => z != null)
  }

  val getEntityWithRecommendation = (dimensionsList: mutable.WrappedArray[mutable.WrappedArray[String]], dimensionSeparator: String, dimensionBasedRecommendation: DimensionBasedRecommendation)=>{

    var flattenDimArray: ArrayBuffer[String] = ArrayBuffer[String]()
    for(dim <- dimensionsList)
      flattenDimArray.addAll(JavaConversions.seqAsJavaList(dim))

    val entityValues = getEntityValuesFromDimension(flattenDimArray, dimensionBasedRecommendation.dimension1, dimensionBasedRecommendation.dimension2, dimensionSeparator)

    dimensionBasedRecommendation.withEntities(entityValues.channel, entityValues.contentType, entityValues.product, entityValues.indication)
  }


  val getEntityValuesFromDimension = (dimensions: ArrayBuffer[String], dim1: String, dim2: String, dimensionSeparator: String) => {
    val dim1elements = dim1.split(dimensionSeparator)
    val dim2elements = dim2.split(dimensionSeparator)
    val entityValues = dim1elements ++ dim2elements
    var index = 0

    var channel: String = null
    var contentType: String = null
    var product: String = null
    var indication: String = null

    for(entityName <- dimensions) {
      if (entityName == "CHANNEL")
        channel = entityValues(index)
      if (entityName == "CONTENTTYPE")
        contentType = entityValues(index)
      if (entityName == "PRODUCT")
        product = entityValues(index)
      if (entityName == "INDICATION")
        indication = entityValues(index)
      index = index + 1
    }
    DimensionEntity(channel, contentType, product, indication)
  }
}
