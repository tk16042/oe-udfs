package com.zsassociates.oe.util

import com.zsassociates.oe.udfs.functions
import com.zsassociates.spark.{SparkUDFRegistrar, SparkUDFRegistry}

class OEUdfRegistrar extends SparkUDFRegistrar{
  override def registerUdfs(registery: SparkUDFRegistry): Unit ={
    registery.register("getMultiDimensionalMLRationaleInfo", functions.getMultiDimensionalMLRationaleInfo)
    registery.register("getContentMultidimensional", functions.getContentMultidimensional)
    registery.register("getContentCodeMultiDimensional", functions.getContentCodeMultiDimensional)
    registery.register("completeMinMaxAllowedTouchpoints", functions.completeMinMaxAllowedTouchpoints)
    registery.register("getContentCode",functions.getContentCode)
    registery.register("getContent", functions.getContent)
    registery.register("getMLRationaleInfo",functions.getMLRationaleInfo)
    registery.register("filterFixedEventsByDate",functions.filterFixedEventsByDate)
    registery.register("getContentType",functions.getContentType)
  }
}
